/*
 * Copyright 2017 by Walid YAICH <walid.yaich@esprit.tn>
 * This is an Open Source Software
 * License: http://www.gnu.org/licenses/gpl.html GPL version 3
 */

package tn.esprit.spring.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;


@Entity
public class Membre {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long cin;
	
	private String nom;
	
	private String prenom;
	
	private Boolean isActif;
	
	@ManyToMany(cascade=CascadeType.MERGE)
	private List<Club> clubs;
	
	@OneToOne
	private Club clubPrefere;

	
	public Long getCin() {
		return cin;
	}

	public void setCin(Long cin) {
		this.cin = cin;
	}

	public Boolean getIsActif() {
		return isActif;
	}

	public void setIsActif(Boolean isActif) {
		this.isActif = isActif;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public List<Club> getClubs() {
		return clubs;
	}

	public void setClubs(List<Club> clubs) {
		this.clubs = clubs;
	}

	public Club getClubPrefere() {
		return clubPrefere;
	}

	public void setClubPrefere(Club clubPrefere) {
		this.clubPrefere = clubPrefere;
	}
	
}
