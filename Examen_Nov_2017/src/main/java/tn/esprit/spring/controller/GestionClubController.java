/*
 * Copyright 2017 by Walid YAICH <walid.yaich@esprit.tn>
 * This is an Open Source Software
 * License: http://www.gnu.org/licenses/gpl.html GPL version 3
 */

package tn.esprit.spring.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import tn.esprit.spring.entity.Club;
import tn.esprit.spring.entity.Membre;
import tn.esprit.spring.repository.ClubRepository;
import tn.esprit.spring.repository.MembreRepository;

@RestController
@RequestMapping("/api/gestionClub")
public class GestionClubController {

	@Autowired
	private ClubRepository clubRepository;
	
	@Autowired
	private MembreRepository membreRepository;
	
	
	/**
	 * Ajouter un club
	 * @param club
	 */
	@RequestMapping(value="/club", method=RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> ajouterClub(@RequestBody Club club){
		clubRepository.save(club);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
	
	
	/**
	 * Récupérer un club
	 * @param clientId
	 */
	@RequestMapping(value="/club/{clubId}", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Club> recupererClub(@PathVariable("clubId") Long clubId){
		Club club = clubRepository.findOne(clubId);
		if(club == null){
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(club, HttpStatus.OK);
	}
	
	
	/**
	 * Récupérer un membre
	 * @param membreId
	 * @return
	 */
	@RequestMapping(value="/membre/{membreId}", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Membre> recupererMembre(@PathVariable("membreId") Long membreId){
		Membre membre = membreRepository.findOne(membreId);
		if(membre == null){
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(membre, HttpStatus.OK);
	}
	
	
	/**
	 * Ajouter un membre et l'affecter a un club
	 * @param membre
	 * @param idClub
	 */
	@RequestMapping(value="/membre", method=RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE )
	public ResponseEntity<Void> creerMembreEtAffecterAClub(@RequestBody Membre membre,
														@RequestParam("idClub") Long idClub){
		ArrayList<Club> clubs = new ArrayList<>();
		clubs.add(new Club(idClub));
		membre.setClubs(clubs);
		membreRepository.save(membre);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	

	/**
	 * Cette resource permet a un membre de choisir son club préféré
	 * @param idClub
	 * @param membreId
	 * @return
	 */
	@RequestMapping(value="/membre/{membreId}", method=RequestMethod.PUT)
	public ResponseEntity<Void> choisirClubPrefere(@RequestParam("idClubPrefere") Long idClub,
			@PathVariable("membreId") Long membreId){
		Membre membre = membreRepository.findOne(membreId);
		membre.setClubPrefere(new Club(idClub));
		membreRepository.save(membre);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	

	/**
	 * Supprimer un membre
	 * @param membreId
	 */
	@RequestMapping(value="/membre/{membreId}", method=RequestMethod.DELETE)
	ResponseEntity<Void> supprimerMembre(@PathVariable("membreId") Long membreId){
		membreRepository.delete(membreId);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	
}